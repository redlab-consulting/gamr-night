﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime

COPY Server/bin/Release/net5.0/publish/ App/
WORKDIR /App

ENTRYPOINT ["dotnet", "gamr-night.Server.dll"]
